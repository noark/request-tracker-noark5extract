#!/usr/bin/python
# -*- coding: utf-8 -*-
# Copyright (c) 2018 Petter Reinholdtsen <pere@hungry.com>
# This file is covered by the GPLv2 or later, read COPYING for details.

from distutils.core import setup

setup(
    name='request-tracker-noark5extract',
    version='0.0',
    scripts=['bin/request-tracker-noark5extract'],
)
