How RT is mapped to Noark 5
===========================

Here is a summary on how Request Trackerinformation is mapped to
Noark 5.

Everything fetched from RT is stored as JSON files in separate
attachments, to ensure no information is lost in the conversion
compared to the information extracted from RT.

* arkiv = RT instance
* arkiv.systemid = RT URL (https://rt.nuug.no/)

* arkivdel = RT queue
* arkivdel.systemid = RT queue name (general)

* mappe = ticket
* mappe.systemID = URL to RT ticket display (https://rt.nuug.no/Ticket/Display.html?id=1)
* mappe.mappeID = RT ticket ID number (1)
* mappe.tittel = RT ticket field 'Subject' (Get started with RT)
* mappe.opprettetDato = RT ticket field 'Created' in ISO 8601 format
* mappe.opprettetAv = RT ticket first event field 'Creator' (someone@example.com)
* mappe.avsluttetDato = RT ticket last event field 'Created' in ISO 8601 format
* mappe.avsluttetAv = RT ticket last event field 'Creator' (someone@example.com)

* registrering = RT ticket history event
* registrering.opprettetDato = event field 'Created' in ISO 8601 format
* registrering.opprettetAv = event field 'Creator'
* registrering.arkivertDato = event field 'Created' in ISO 8601 format
* registrering.arkivertAv = event field 'Creator'
* registrering.registreringsID = event field 'id'
* registrering.tittel = event field 'Description'

* dokumentbeskrivelse = RT ticket attachment
* dokumentbeskrivelse.tittel = RT attachment name or extracted JSON name name

* dokumentobjekt =  Rt ticket attachment
* dokumentobjekt.filstoerrelse = attachment size
* dokumentobjekt.format = string derived from attachment MIME type and file content
* dokumentobjekt.referanseDokumentfil = relative path to file with attachment content
* dokumentobjekt.sjekksum = generated SHA-256 hexdigest of attachment content
* dokumentobjekt.sjekksumAlgoritme = string 'SHA-256'
* dokumentobjekt.variantformat = string 'Produksjonsformat'
* dokumentobjekt.versjonsnummer = string '1'
