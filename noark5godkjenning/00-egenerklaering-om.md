# Egenerklæringsskjema for godkjenning av Noark 5-løsning

## Overordnet informasjon om løsningen

### Betegnelse/navn på løsningen

Request Tracker Noark 5 Extract

### Kort beskrivelse av løsningen

Selvstendig system for Noark 5-uttrekk av dokumenter og av saker
lagret i Request Tracker ved hjelp av API-et til Request Tracker.

Systemet er tilgjengelig med fri programvarevilkår fra
https://gitlab.com/petterreinholdtsen/request-tracker-noark5extract .

### Navn på leverandør/organ som har utviklet systemet

Petter Reinholdtsen

### Kontaktinformasjon og adresse til leverandør/organ som har utviklet systemet

Kan kontaktes via gitlab-prosjektet.

### Opplysningene er utfylt av (navn, tittel, rolle og arbeidsstted):

Petter Reinholdtsen, fri programvareutvikler

### Sted, dato og signatur

Oslo, 2020-MM-DD

Petter Reinholdtsen

## Type Løsning

* X Enkel arkivstruktur (ikke sakarkiv)
* . Arkivstruktur med møtemappe og møteregistrering
* . Komplett arkivstruktur (sakarkiv med saksmappe og journalpost)
* . Håndtering av parter
* . Presedens
* . Dokumentflyt (godkjenning)
* . Avskrivning
* . Bevaring og kassasjon
* X Avlevering (produksjon av arkivuttrekk)
* . Integrasjon ved hjelp av GeoIntegrasjonsstandarden
* . Integrasjon ved hjelp av Noark 5 tjenestegrensesnittet
